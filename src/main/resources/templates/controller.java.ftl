package ${package.Controller};


import org.springframework.web.bind.annotation.*;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import javax.annotation.Resource;

import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
import ${cfg.basePackage}.struct.PageResult;
import ${cfg.basePackage}.struct.Result;
import ${cfg.basePackage}.vo.SearchVO;

import java.util.List;

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>
    @Resource
	private ${table.serviceName} ${table.entityPath}Service;

    @GetMapping("/{id}")
    public Result<${entity}> getById(@PathVariable("id") Long id) {
        return Result.ok(${table.entityPath}Service.getById(id));
    }

    @GetMapping
    public Result<List<${entity}>> list() {
        return Result.ok(${table.entityPath}Service.list());
    }

    @PostMapping
    public Result add(@RequestBody ${entity} ${table.entityPath}) {
        return ${table.entityPath}Service.save${entity}(${table.entityPath}) ? Result.ok() : Result.error();
    }

    @PutMapping
    public Result update(@RequestBody ${entity} ${table.entityPath}) {
        return ${table.entityPath}Service.update${entity}(${table.entityPath}) ? Result.ok() : Result.error();
    }

    @DeleteMapping("/{id}")
    public Result delById(@PathVariable("id") Long id) {
        return ${table.entityPath}Service.delById(id) ? Result.ok() : Result.error();
    }

    @PostMapping("search/{currPage}/{pageSize}")
    public Result<PageResult<${entity}>> page(@PathVariable("currPage") Integer currPage,
                                            @PathVariable("pageSize") Integer pageSize,
                                            @RequestBody(required = false) SearchVO vo) {
        return Result.ok(${table.entityPath}Service.page(currPage, pageSize, vo));
    }

    @PostMapping("delByIds")
    public Result delByIds(@RequestBody List<Long> ids) {
        return ${table.entityPath}Service.delByIds(ids) ? Result.ok() : Result.error();
    }

}
</#if>
