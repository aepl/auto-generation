package org.laobai.generation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class GenerationStarter {
    @Value("${bp.author}")
    private String author;
    @Value("${bp.user}")
    private String user;
    @Value("${bp.pwd}")
    private String pwd;
    @Value("${bp.packageName}")
    private String packageName;
    @Value("${bp.dbName}")
    private String dbName;
    @Value("${bp.path}")
    private String path;

    @Value("${bp.isMain}")
    private boolean isMain;


    public static void main(String[] args) {
        SpringApplication.run(GenerationStarter.class, args);
    }

    @PostConstruct
    public void init(){
        CodeGeneratorWithForm codeGenerator = new CodeGeneratorWithForm(isMain);
        codeGenerator.init();
        codeGenerator.myEvent();
        codeGenerator.disposeArgs(author,user,pwd,packageName,dbName,path);
    }
}
