package ${package.Service};

import ${package.Entity}.${entity};
import ${cfg.basePackage}.struct.PageResult;
import ${cfg.basePackage}.vo.SearchVO;

import ${superServiceClassPackage};
import java.util.List;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    /**
    * 分页列表查询
    * @param currPage
    * @param pageSize
    * @param vo
    * @return
    */
    PageResult<${entity}> page(Integer currPage, Integer pageSize, SearchVO vo);

    /**
    * 添加${table.comment!}
    * @param ${table.entityPath}
    * @return
    */
    boolean save${entity}(${entity} ${table.entityPath});

    /**
    * 修改${table.comment!}
    * @param ${table.entityPath}
    * @return
    */
    boolean update${entity}(${entity} ${table.entityPath});

    /**
    * 根据ID删除${table.comment!}
    * @param id
    * @return
    */
    boolean delById(Long id);

    /**
    * 根据主键IDs批量删除
    * @param ids
    * @return
    */
    boolean delByIds(List<Long> ids);

}
</#if>
