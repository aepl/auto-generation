package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${cfg.basePackage}.struct.PageResult;
import ${cfg.basePackage}.vo.SearchVO;
import ${cfg.basePackage}.util.StringUtils;
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.Objects;
import java.util.List;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

    @Override
    public PageResult<${entity}> page(Integer currPage, Integer pageSize, SearchVO vo) {
        // 分页
        Page<${entity}> ${table.entityPath}Page = new Page<>(currPage, pageSize);

        // 条件查询
        QueryWrapper<${entity}> wrapper = new QueryWrapper<>();

        if (Objects.nonNull(vo)) {
            String keyWord = vo.getKeyWord();
            if (StringUtils.isNotEmpty(keyWord)) {
                wrapper.like("name", keyWord);
            }
        }
        ${table.entityPath}Page = page(${table.entityPath}Page, wrapper);
        return PageResult.of(${table.entityPath}Page.getRecords(), ${table.entityPath}Page.getTotal());
    }

    @Override
    public boolean save${entity}(${entity} ${table.entityPath}) {
        return save(${table.entityPath});
    }

    @Override
    public boolean update${entity}(${entity} ${table.entityPath}) {
        return updateById(${table.entityPath});
    }

    @Override
    public boolean delById(Long id) {
        return removeById(id);
    }

    @Override
    public boolean delByIds(List<Long> ids) {
        return removeByIds(ids);
    }
}
</#if>
